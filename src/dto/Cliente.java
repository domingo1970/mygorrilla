package dto;
import java.io.Serializable;

/**
 *
 * @author portatil
 */

//variables , getters y setters
public class Cliente implements Serializable{
    private Integer idCliente;
    private String matricula;
    private String modelo;
    private String propina;
    private String accion;
    
   public Cliente(){
   }
   public Integer getIdCliente(){
       return idCliente;
   }
   public void setIdCliente(Integer idCliente){
       this.idCliente=idCliente;
   }
   public String getMatricula(){
       return matricula;
   }
   public void setMatricula(String matricula){
       this.matricula=matricula;
   } 
   public String getModelo(){
       return modelo;
   }
   public void setModelo(String modelo){
       this.modelo=modelo;
   } 
   public String getPropina(){
       return propina;
   }
   public void setPropina(String propina){
       this.propina=propina;   
   }
   public String getAccion(){
       return accion;
   }
   public void setAccion(String accion){
       this.accion=accion;
   }
   
    
}
